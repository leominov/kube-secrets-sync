package main

import (
	"context"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
)

const (
	ManagedByLabel      = "app.kubernetes.io/managed-by"
	ManagedByLabelValue = "kube-secrets-sync"
)

type Operator struct {
	logger    *logrus.Entry
	closed    bool
	clientSet kubernetes.Interface
	c         *Config
	doneCh    chan struct{}
	watcher   watch.Interface
}

func NewOperator(logger *logrus.Entry, clientSet kubernetes.Interface, config *Config) (*Operator, error) {
	listOptions := config.KubernetesListOptions()
	watcher, err := clientSet.CoreV1().Secrets(config.SourceNamespace).Watch(context.TODO(), listOptions)
	if err != nil {
		return nil, err
	}

	operator := &Operator{
		logger:    logger,
		clientSet: clientSet,
		doneCh:    make(chan struct{}),
		c:         config,
		watcher:   watcher,
	}

	return operator, nil
}

func (o *Operator) Watch() {
	ticker := time.NewTicker(o.c.SyncInterval)
	go o.serveMetrics()
	for {
		select {
		case event := <-o.watcher.ResultChan():
			o.eventHandler(event)
		case <-ticker.C:
			err := o.syncNamespaces()
			if err != nil {
				logrus.WithError(err).Error("Complete sync failed")
			}
		case <-o.doneCh:
			ticker.Stop()
			o.watcher.Stop()
			break
		}
	}
}

func (o *Operator) syncNamespaces() error {
	listOptions := o.c.KubernetesListOptions()
	secretList, err := o.clientSet.CoreV1().Secrets(o.c.SourceNamespace).List(context.TODO(), listOptions)
	if err != nil {
		return err
	}
	var lastSyncErr error
	for _, secret := range secretList.Items {
		for _, ns := range o.c.DestinationNamespaces {
			err = o.syncNamespaceWithSecret(ns, &secret)
			if err != nil {
				lastSyncErr = err
			}
		}
	}
	return lastSyncErr
}

func (o *Operator) serveMetrics() {
	if o.c.ListenAddress == "" {
		return
	}
	o.logger.Infof("Telemetry address: %s", o.c.ListenAddress)
	http.Handle("/metrics", promhttp.Handler())
	if err := http.ListenAndServe(o.c.ListenAddress, nil); err != nil {
		logrus.WithError(err).Error("Failed to start metrics server")
	}
}

func (o *Operator) syncNamespaceWithSecret(namespace string, secret *corev1.Secret) error {
	secretsInterface := o.clientSet.CoreV1().Secrets(namespace)
	s, err := secretsInterface.Get(context.Background(), secret.Name, metav1.GetOptions{})
	if err != nil {
		newSecret := secret.DeepCopy()
		newSecret.ObjectMeta.Namespace = namespace
		newSecret.ResourceVersion = ""
		addServiceLabelsToSecret(newSecret)
		_, err = secretsInterface.Create(context.Background(), newSecret, metav1.CreateOptions{})
		if err != nil {
			lastActionSuccessful.WithLabelValues("create", namespace, newSecret.Name).Set(0)
			return err
		}
		lastActionSuccessTimestamp.WithLabelValues("create", namespace, newSecret.Name).SetToCurrentTime()
		lastActionSuccessful.WithLabelValues("create", namespace, newSecret.Name).Set(1)
		o.logger.Infof("%s/%s secret created", namespace, newSecret.Name)
		return nil
	}

	s.Data = secret.Data
	addServiceLabelsToSecret(s)
	_, err = secretsInterface.Update(context.Background(), s, metav1.UpdateOptions{})
	if err != nil {
		lastActionSuccessful.WithLabelValues("update", namespace, s.Name).Set(0)
		return err
	}
	lastActionSuccessTimestamp.WithLabelValues("update", namespace, s.Name).SetToCurrentTime()
	lastActionSuccessful.WithLabelValues("update", namespace, s.Name).Set(1)
	return nil
}

func (o *Operator) eventHandler(event watch.Event) {
	accessor, err := meta.Accessor(event.Object)
	if err != nil {
		return
	}

	if event.Type != watch.Added && event.Type != watch.Modified {
		return
	}

	o.logger.Infof("Handle %s/%s %s event", accessor.GetNamespace(), accessor.GetName(), event.Type)

	secret, err := o.clientSet.CoreV1().Secrets(accessor.GetNamespace()).Get(context.Background(), accessor.GetName(), metav1.GetOptions{})
	if err != nil {
		lastActionSuccessful.WithLabelValues("read", accessor.GetNamespace(), accessor.GetName()).Set(0)
		o.logger.WithError(err).Errorf("Failed to get %s/%s secret", accessor.GetNamespace(), accessor.GetName())
		return
	}

	lastActionSuccessTimestamp.WithLabelValues("read", accessor.GetNamespace(), accessor.GetName()).SetToCurrentTime()
	lastActionSuccessful.WithLabelValues("read", accessor.GetNamespace(), accessor.GetName()).Set(1)

	for _, ns := range o.c.DestinationNamespaces {
		err := o.syncNamespaceWithSecret(ns, secret)
		if err != nil {
			o.logger.WithError(err).Errorf("Failed to create %s/%s secret", ns, secret.Name)
		} else {
			o.logger.Infof("%s/%s secret synced", ns, secret.Name)
		}
	}
}

// ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/#labels
func addServiceLabelsToSecret(secret *corev1.Secret) {
	if secret.ObjectMeta.Labels == nil {
		secret.ObjectMeta.Labels = make(map[string]string)
	}
	secret.ObjectMeta.Labels[ManagedByLabel] = ManagedByLabelValue
}

func (o *Operator) Stop() {
	if o.closed {
		return
	}
	o.closed = true
	close(o.doneCh)
}
