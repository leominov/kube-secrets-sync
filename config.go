package main

import (
	"errors"
	"time"

	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v2"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type Config struct {
	ListenAddress string `envconfig:"LISTEN_ADDRESS" yaml:"listen_address" default:":8080"`
	LogLevel      string `envconfig:"LOG_LEVEL" yaml:"log_level" default:"info"`

	// DestinationNamespaces список неймспейсов, куда будут синхронизироваться секреты
	DestinationNamespaces []string `envconfig:"DESTINATION_NAMESPACES" yaml:"destination_namespaces" required:"true"`

	// SourceNamespace исходный неймспейс с секретами и селекторы
	SourceNamespace            string `envconfig:"SOURCE_NAMESPACE" yaml:"source_namespace" required:"true"`
	SourceSecretsFieldSelector string `envconfig:"SOURCE_SECRETS_FIELD_SELECTOR" yaml:"source_secrets_field_selector"`
	SourceSecretsLabelSelector string `envconfig:"SOURCE_SECRETS_LABEL_SELECTOR" yaml:"source_secrets_label_selector"`

	// SyncInterval интервал синхронизации на случай, если событие не было получено и мы не
	// зафиксировали сигнал о необходимости обновить секреты в DestinationNamespaces
	SyncInterval time.Duration `envconfig:"SYNC_INTERVAL" yaml:"sync_interval" default:"24h"`
}

func LoadConfigFromEnv() (*Config, error) {
	c := &Config{}
	err := envconfig.Process("", c)
	if err != nil {
		return nil, err
	}

	if c.SourceSecretsFieldSelector == "" && c.SourceSecretsLabelSelector == "" {
		return nil, errors.New("empty source secrets selector")
	}

	// Слишком частая синхронизация приведет к постоянному обращению API, если такое
	// требуется, значит что-то настроено не так, в нормальном режиме проверка раз в
	// сутки выглядит нормальным
	if c.SyncInterval < time.Hour {
		c.SyncInterval = 24 * time.Hour
	}

	for _, destNamespace := range c.DestinationNamespaces {
		if c.SourceNamespace == destNamespace {
			return nil, errors.New("source and destination namespace can't be equal")
		}
	}
	return c, nil
}

func (c *Config) String() string {
	b, _ := yaml.Marshal(c)
	return string(b)
}

func (c *Config) KubernetesListOptions() metav1.ListOptions {
	listOptions := metav1.ListOptions{}

	if c.SourceSecretsLabelSelector != "" {
		listOptions.LabelSelector = c.SourceSecretsLabelSelector
	}
	if c.SourceSecretsFieldSelector != "" {
		listOptions.FieldSelector = c.SourceSecretsFieldSelector
	}

	return listOptions
}

func PrintsUsage() {
	c := &Config{}
	_ = envconfig.Usage("", c)
}
