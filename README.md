# kube-secrets-sync

* [![App Status](https://argo.infra.cloud.qlean.ru/api/badge?name=kube-secrets-sync-preprod&revision=true)](https://argo.infra.cloud.qlean.ru/applications/kube-secrets-sync-preprod) – preprod
* [![App Status](https://argo.infra.cloud.qlean.ru/api/badge?name=kube-secrets-sync-prod&revision=true)](https://argo.infra.cloud.qlean.ru/applications/kube-secrets-sync-prod) – prod
* [![App Status](https://argo.infra.cloud.qlean.ru/api/badge?name=kube-secrets-sync-stage&revision=true)](https://argo.infra.cloud.qlean.ru/applications/kube-secrets-sync-stage) – stage

Копирует секреты (по LabelSelector или FieldSelector) из одного namespace в другие.

## Переменные окружения

* `DESTINATION_NAMESPACES` – список namespace, куда будут копироваться секреты (обязательно);
* `LISTEN_ADDRESS` – адрес для телеметрии (`:8080`);
* `LOG_LEVEL` – уровень логирования (`info`);
* `SOURCE_NAMESPACE` – namespace с исходными секретами (обязательно);
* `SOURCE_SECRETS_FIELD_SELECTOR` – FieldSelector для секретов (один из селекторов обязателен);
* `SOURCE_SECRETS_LABEL_SELECTOR` – LabelSelector для секретов.

## Ссылки

* https://kubernetes.io/docs/concepts/overview/working-with-objects/field-selectors/
* https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/
