package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadConfigFromEnv(t *testing.T) {
	_ = os.Unsetenv("SOURCE_NAMESPACE")
	_ = os.Unsetenv("SOURCE_SECRETS_FIELD_SELECTOR")
	_ = os.Unsetenv("SOURCE_SECRETS_LABEL_SELECTOR")
	_ = os.Unsetenv("DESTINATION_NAMESPACES")
	_, err := LoadConfigFromEnv()
	assert.Error(t, err)

	_ = os.Setenv("DESTINATION_NAMESPACES", "foobar")
	_ = os.Setenv("SOURCE_NAMESPACE", "default")
	_, err = LoadConfigFromEnv()
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "empty source")
	}

	_ = os.Setenv("SOURCE_SECRETS_FIELD_SELECTOR", "metadata.name=secret")
	_, err = LoadConfigFromEnv()
	assert.NoError(t, err)

	_ = os.Setenv("DESTINATION_NAMESPACES", "default")
	_, err = LoadConfigFromEnv()
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "can't be equal")
	}

	_ = os.Setenv("DESTINATION_NAMESPACES", "foobar,barfoo")
	config, err := LoadConfigFromEnv()
	if assert.NoError(t, err) {
		assert.Equal(t, []string{"foobar", "barfoo"}, config.DestinationNamespaces)
	}
}

func TestConfig_String(t *testing.T) {
	c := &Config{}
	assert.NotEmpty(t, c.String())
}

func TestPrintsUsage(t *testing.T) {
	PrintsUsage()
}
