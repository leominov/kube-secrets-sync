package main

import "github.com/prometheus/client_golang/prometheus"

var (
	lastActionSuccessTimestamp = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "kube_secrets_sync",
		Subsystem: "last_action",
		Name:      "success_timestamp_seconds",
		Help:      "Timestamp of last action.",
	}, []string{"action", "namespace", "secret"})

	lastActionSuccessful = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "kube_secrets_sync",
		Subsystem: "last_action",
		Name:      "successful",
		Help:      "Status of last action.",
	}, []string{"action", "namespace", "secret"})
)

func init() {
	prometheus.MustRegister(
		lastActionSuccessTimestamp,
		lastActionSuccessful,
	)
}
