package main

import (
	"context"
	"reflect"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
)

func TestOperator_Watch(t *testing.T) {
	k8sClient := fake.NewSimpleClientset(
		&corev1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: "namespace-a",
			},
		},
		&corev1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: "namespace-b",
			},
		},
		&corev1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: "namespace-c",
			},
		},
	)
	config := &Config{
		SourceNamespace:            "namespace-a",
		SourceSecretsFieldSelector: "metadata.name=secret",
		SourceSecretsLabelSelector: "foo=bar",
		DestinationNamespaces: []string{
			"namespace-b",
			"namespace-c",
		},
		SyncInterval: 24 * time.Hour,
	}
	logger := logrus.New().WithField("env", "test")
	operator, err := NewOperator(logger, k8sClient, config)
	if !assert.NoError(t, err) {
		return
	}
	go operator.Watch()
	defer operator.Stop()

	time.Sleep(100 * time.Millisecond)

	originSecret, err := k8sClient.CoreV1().Secrets("namespace-a").Create(context.Background(), &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name: "secret",
			Labels: map[string]string{
				"foo": "bar",
			},
		},
		Data: map[string][]byte{
			"foo": []byte("bar"),
		},
	}, metav1.CreateOptions{})
	if !assert.NoError(t, err) {
		return
	}

	time.Sleep(100 * time.Millisecond)

	for _, ns := range config.DestinationNamespaces {
		secret, err := k8sClient.CoreV1().Secrets(ns).Get(context.Background(), "secret", metav1.GetOptions{})
		if !assert.NoError(t, err) {
			return
		}
		assert.True(t, reflect.DeepEqual(secret.Data, originSecret.Data))
	}

	originSecret.Data["bar"] = []byte("foo")
	originSecret, err = k8sClient.CoreV1().Secrets("namespace-a").Update(context.Background(), originSecret, metav1.UpdateOptions{})
	if !assert.NoError(t, err) {
		return
	}
	_, ok := originSecret.Data["foo"]
	assert.True(t, ok)
	_, ok = originSecret.Data["bar"]
	assert.True(t, ok)

	time.Sleep(100 * time.Millisecond)

	for _, ns := range config.DestinationNamespaces {
		secret, err := k8sClient.CoreV1().Secrets(ns).Get(context.Background(), "secret", metav1.GetOptions{})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, ManagedByLabelValue, secret.Labels[ManagedByLabel])
		assert.True(t, reflect.DeepEqual(secret.Data, originSecret.Data))
	}

	err = k8sClient.CoreV1().Secrets("namespace-a").Delete(context.Background(), "secret", metav1.DeleteOptions{})
	assert.NoError(t, err)
	err = k8sClient.CoreV1().Secrets("namespace-a").Delete(context.Background(), "secret", metav1.DeleteOptions{})
	assert.Error(t, err)

	time.Sleep(100 * time.Millisecond)

	err = k8sClient.CoreV1().Secrets("namespace-b").Delete(context.Background(), "secret", metav1.DeleteOptions{})
	assert.NoError(t, err)
	err = k8sClient.CoreV1().Secrets("namespace-c").Delete(context.Background(), "secret", metav1.DeleteOptions{})
	assert.NoError(t, err)

	operator.Stop()
}
