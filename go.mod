module gitlab.qleanlabs.ru/platform/infra/kube-secrets-sync

go 1.16

require (
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/prometheus/client_golang v1.11.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	gopkg.in/yaml.v2 v2.4.0
	k8s.io/api v0.22.1
	k8s.io/apimachinery v0.22.1
	k8s.io/client-go v0.22.1
)
