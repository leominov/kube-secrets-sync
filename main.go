package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
)

var (
	help     = flag.Bool("help", false, "Prints help and exit.")
	validate = flag.Bool("validate", false, "Validate configuration end exit.")
)

func main() {
	flag.Parse()

	if *help {
		PrintsUsage()
		return
	}

	l := logrus.New()
	l.SetFormatter(&logrus.JSONFormatter{})
	logger := l.WithField("app", "kube-secrets-sync")

	config, err := LoadConfigFromEnv()
	if err != nil {
		logger.Fatal(err)
	}

	if *validate {
		fmt.Println(config.String())
		return
	}

	if level, err := logrus.ParseLevel(config.LogLevel); err == nil {
		l.SetLevel(level)
	}

	logger.Info("Watcher initialization...")

	kubeClientSet, err := NewClientSet()
	if err != nil {
		logger.Fatal(err)
	}

	operator, err := NewOperator(logger, kubeClientSet, config)
	if err != nil {
		logger.Fatal(err)
	}

	logger.Info("Watching for incoming events...")

	go operator.Watch()

	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, syscall.SIGINT, syscall.SIGTERM)

	s := <-signalCh
	logger.Infof("Captured %v. Exiting...", s)
	operator.Stop()
}
